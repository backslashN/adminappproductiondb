package admin.lokacart.ict.mobile.com.adminapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;

import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v4.app.FragmentManager;

import admin.lokacart.ict.mobile.com.adminapp.R;

/**
 * Created by root on 19/1/16.
 */
public class OrderFragment extends Fragment {
    PagerAdapter mPageAdapter;
    ViewPager viewPager;
    FragmentManager fm;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab, container, false);
        getActivity().setTitle(R.string.title_orders);
        setRetainInstance(true);

        final TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_pending_order_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_processed_order_fragment)));
        tabLayout.addTab(tabLayout.newTab().setText(getResources().getString(R.string.label_tab_cancelled_order_fragment)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        fm = getFragmentManager();

        mPageAdapter = new PagerAdapter(fm, tabLayout.getTabCount(),"Order");

        viewPager = (ViewPager)rootView.findViewById(R.id.pager);
        viewPager.setAdapter(mPageAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        //viewPager.setOffscreenPageLimit(0);
        viewPager.setCurrentItem(0);

        try
        {
            Bundle bundle = this.getArguments();
            if(bundle.get("to").equals("processed"))
                viewPager.setCurrentItem(1);
            else if(bundle.get("to").equals("cancelled"))
                viewPager.setCurrentItem(2);
        }
        catch (Exception e)
        {
            viewPager.setCurrentItem(0);
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                DashboardActivity.resetBackPress();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        return rootView;
    }
}

