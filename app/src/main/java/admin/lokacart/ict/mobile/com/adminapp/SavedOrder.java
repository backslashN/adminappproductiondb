package admin.lokacart.ict.mobile.com.adminapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.TreeMap;

public class SavedOrder {

    String userName;
    int orderId;
    String timeStamp;
    String comment;
    TreeMap<String,String[]> itemList;

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public SavedOrder(JSONObject order, int pos)
    {
    	try
        {
			this.orderId=order.getInt("orderid");
            this.userName=order.getString("username");
			this.timeStamp=order.getString("timestamp");
            try
            {
                this.comment=order.getString("comment");
            }
            catch (JSONException e)
            {
                this.comment="No Comments";
            }
			JSONArray orderItems=order.getJSONArray("items");
            this.itemList=new TreeMap<String, String[]>();
            for(int i=0;i<orderItems.length();i++){
				JSONObject item=(JSONObject)orderItems.get(i);
                String name=item.getString("productname");
                String[] temp=new String[2];
                temp[0]=item.getString("rate");
                temp[1]=item.getString("quantity");
                this.itemList.put(pos+name,temp);
			}
		}
        catch (Exception e)
        {
			e.printStackTrace();
		}
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getItems()
    {
    	String res="";
    	for(String itemName:itemList.keySet())
        {
    		res+= itemName + "-" + itemList.get(itemName)[1] + "\n";
    	}
    	return res;
    }

    public ArrayList<String[]> getItemsList(int pos)
    {
        ArrayList<String[]> itemlist = new ArrayList<>();
        String[] res = new String[3] ;
        int posLen = (""+pos).length();
        for (String itemName:itemList.keySet())
        {
            res = new String[3] ;
            String name =itemName.substring(posLen);
            res[0]=name;
            res[1]=itemList.get(itemName)[0];
            res[2]=itemList.get(itemName)[1];
            double rate= Double.parseDouble(itemList.get(itemName)[0]);
            double quantity= Double.parseDouble(itemList.get(itemName)[1]);
            itemlist.add(res);
        }
        return itemlist;
    }
    public TreeMap<String,String[]> getItemList() {
    	return itemList;
    }
}