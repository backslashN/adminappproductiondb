
package admin.lokacart.ict.mobile.com.adminapp.fragments;

/**
 * Created by Vishesh on 19-01-2016.
 */
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import admin.lokacart.ict.mobile.com.adminapp.AdminDetails;
import admin.lokacart.ict.mobile.com.adminapp.DashboardActivity;
import admin.lokacart.ict.mobile.com.adminapp.GetJSON;
import admin.lokacart.ict.mobile.com.adminapp.Master;
import admin.lokacart.ict.mobile.com.adminapp.R;
import admin.lokacart.ict.mobile.com.adminapp.NavigationItemListener;

public class DashboardFragment extends Fragment implements View.OnClickListener{

    TextView tOrders, tMembers, tPendingOrders, tProcessedOrders, tCancelledOrders, tTotalUsers, tNewUsersToday, tPendingRequests;
    int pendingOrders, processedOrders, cancelledOrders, totalUsers, newUsersToday, pendingRequests;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    NavigationItemListener callback;
    View dashboardFragmentView;
    static boolean po, pro, co, tu, nut, pr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        dashboardFragmentView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        getActivity().setTitle(R.string.title_dashboard);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        editor = sharedPreferences.edit();
        po = pro = co = tu = nut = pr = false;
        callback = (NavigationItemListener) getActivity();
        callback.itemSelected(Master.getDashboardTAG());
        return dashboardFragmentView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        tOrders = (TextView) dashboardFragmentView.findViewById(R.id.tDashboardFragmentOrders);
        tMembers = (TextView) dashboardFragmentView.findViewById(R.id.tDashboardFragmentMembers);

        tPendingOrders = (TextView) dashboardFragmentView.findViewById(R.id.tPendingOrders);
        tPendingOrders.setOnClickListener(this);

        tProcessedOrders = (TextView) dashboardFragmentView.findViewById(R.id.tProcessedOrders);
        tProcessedOrders.setOnClickListener(this);

        tCancelledOrders = (TextView) dashboardFragmentView.findViewById(R.id.tCancelledOrders);
        tCancelledOrders.setOnClickListener(this);

        tTotalUsers = (TextView) dashboardFragmentView.findViewById(R.id.tTotalUsers);
        tTotalUsers.setOnClickListener(this);

        tNewUsersToday = (TextView) dashboardFragmentView.findViewById(R.id.tNewUsersToday);
        tNewUsersToday.setOnClickListener(this);

        tPendingRequests = (TextView) dashboardFragmentView.findViewById(R.id.tPendingRequests);
        tPendingRequests.setOnClickListener(this);

        new GetDashboardDetailsTask().execute();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View v) {
        DashboardActivity.resetBackPress();
        OrderFragment orderFragment = new OrderFragment();
        MemberFragment memberFragment = new MemberFragment();
        Bundle bundle = new Bundle();
        DashboardActivity.onBackDashboard = true;

        switch (v.getId())
        {
            case R.id.tPendingOrders:
                po = false;
                updatePO();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new OrderFragment()).commit();
                callback.itemSelected(Master.getOrdersTAG());
                break;

            case R.id.tProcessedOrders:
                pro = false;
                updatePR();
                bundle.putString("to", "processed");
                orderFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, orderFragment).commit();
                callback.itemSelected(Master.getOrdersTAG());
                break;

            case R.id.tCancelledOrders:
                co = false;
                updateCO();
                bundle.putString("to", "cancelled");
                orderFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, orderFragment).commit();
                callback.itemSelected(Master.getOrdersTAG());
                break;

            case R.id.tTotalUsers:
                tu = false;
                updateTU();
                bundle.putString("to", "existing");
                memberFragment.setArguments(bundle);
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, memberFragment, DashboardActivity.MEMBER_TAG).commit();
                callback.itemSelected(Master.getMembersTAG());
                break;

            case R.id.tNewUsersToday:
                nut = false;
                updateNUT();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new MemberFragment(), DashboardActivity.MEMBER_TAG).commit();
                callback.itemSelected(Master.getMembersTAG());
                break;

            case R.id.tPendingRequests:
                pr = false;
                updatePR();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, new MemberFragment(), DashboardActivity.MEMBER_TAG).commit();
                callback.itemSelected(Master.getMembersTAG());
                break;
        }
    }

    public void update(int params[])
    {
        if(params[0] != sharedPreferences.getInt("pendingOrders", 0))
        {
            tPendingOrders.setText(params[0] + "");
            if(params[0] > sharedPreferences.getInt("pendingOrders", 0))
                po = true;
            else
                po = false;
            updatePO();
            editor.putInt("pendingOrders", params[0]);
        }
        if(params[1] != sharedPreferences.getInt("processedOrders", 0))
        {
            tProcessedOrders.setText(params[1] + "");
            if(params[1] > sharedPreferences.getInt("processedOrders", 0))
                pro = true;
            else
                pro = false;
            updatePR();
            editor.putInt("processedOrders", params[1]);
        }
        if(params[2] != sharedPreferences.getInt("cancelledOrders", 0))
        {
            tCancelledOrders.setText(params[2] + "");
            if(params[2] > sharedPreferences.getInt("cancelledOrders", 0))
                co = true;
            else
                co = false;
            updateCO();
            editor.putInt("cancelledOrders", params[2]);
        }
        if(params[3] != sharedPreferences.getInt("totalUsers", 0))
        {
            tTotalUsers.setText(params[3] + "");
            if(params[3] > sharedPreferences.getInt("totalUsers", 0))
                tu = true;
            else
                tu = false;
            updateTU();
            editor.putInt("totalUsers", params[3]);
        }
        if(params[4] != sharedPreferences.getInt("newUsersToday", 0))
        {
            tNewUsersToday.setText(params[4] + "");
            if(params[4] > sharedPreferences.getInt("newUsersToday", 0))
                nut = true;
            else
                nut = false;
            updateNUT();
            editor.putInt("newUsersToday", params[4]);
        }
        if(params[5] != sharedPreferences.getInt("pendingRequests", 0))
        {
            tPendingRequests.setText(params[5] + "");
            if(params[5] > sharedPreferences.getInt("pendingRequests", 0))
                pr = true;
            else
                pr = false;
            updatePR();
            editor.putInt("pendingRequests", params[5]);
        }
        editor.commit();
    }

    private void updatePO()
    {
        if(po)
        {
            tPendingOrders.setBackgroundResource(R.mipmap.ic_dashboard_notify);
            tPendingOrders.setTextColor(getResources().getColor(R.color.textColorLight));
        }
        else
        {
            tPendingOrders.setBackgroundResource(R.mipmap.ic_dashboard_icon);
            tPendingOrders.setTextColor(getResources().getColor(R.color.textColorDark));
        }
    }

    private void updatePRO()
    {
        if(pro)
        {
            tProcessedOrders.setBackgroundResource(R.mipmap.ic_dashboard_notify);
            tProcessedOrders.setTextColor(getResources().getColor(R.color.textColorLight));
        }
        else
        {
            tProcessedOrders.setBackgroundResource(R.mipmap.ic_dashboard_icon);
            tProcessedOrders.setTextColor(getResources().getColor(R.color.textColorDark));
        }
    }

    private void updateCO()
    {
        if(co)
        {
            tCancelledOrders.setBackgroundResource(R.mipmap.ic_dashboard_notify);
            tCancelledOrders.setTextColor(getResources().getColor(R.color.textColorLight));
        }
        else
        {
            tCancelledOrders.setBackgroundResource(R.mipmap.ic_dashboard_icon);
            tCancelledOrders.setTextColor(getResources().getColor(R.color.textColorDark));
        }
    }

    private void updateTU()
    {
        if(tu)
        {
            tTotalUsers.setBackgroundResource(R.mipmap.ic_dashboard_notify);
            tTotalUsers.setTextColor(getResources().getColor(R.color.textColorLight));
        }
        else
        {
            tTotalUsers.setBackgroundResource(R.mipmap.ic_dashboard_icon);
            tTotalUsers.setTextColor(getResources().getColor(R.color.textColorDark));
        }
    }

    private void updateNUT()
    {
        if(nut)
        {
            tNewUsersToday.setBackgroundResource(R.mipmap.ic_dashboard_notify);
            tNewUsersToday.setTextColor(getResources().getColor(R.color.textColorLight));
        }
        else
        {
            tNewUsersToday.setBackgroundResource(R.mipmap.ic_dashboard_icon);
            tNewUsersToday.setTextColor(getResources().getColor(R.color.textColorDark));
        }
    }

    private void updatePR()
    {
        if(pr)
        {
            tPendingRequests.setBackgroundResource(R.mipmap.ic_dashboard_notify);
            tPendingRequests.setTextColor(getResources().getColor(R.color.textColorLight));
        }
        else
        {
            tPendingRequests.setBackgroundResource(R.mipmap.ic_dashboard_icon);
            tPendingRequests.setTextColor(getResources().getColor(R.color.textColorDark));
        }
    }

/*--------------------------A class to get the dashboard updates------------------------
    -----------API------------------
    /api/dashboard?orgabbr=iitb

     {
        "processed": 26
        "totalUsers": 138
        "saved": 21
        "cancelled": 30
        "newUsersToday": 0
        "pendingUsers": 114
      }
    --------------------------------*/

    public class GetDashboardDetailsTask extends AsyncTask<String, String, String>
    {

        ProgressDialog pd;
        String response;
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(getActivity());
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params)
        {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDashboardDetailsURL()+ AdminDetails.getAbbr(), null, "GET", true,
                    AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message)
        {
            pd.dismiss();
            if(message.equals("exception"))
            {
                try
                {
                    tPendingOrders.setText(sharedPreferences.getInt("pendingOrders", 0));
                    tProcessedOrders.setText(sharedPreferences.getInt("processedOrders", 0));
                    tCancelledOrders.setText(sharedPreferences.getInt("cancelledOrders", 0));
                    tTotalUsers.setText(sharedPreferences.getInt("totalUsers", 0));
                    tNewUsersToday.setText(sharedPreferences.getInt("newUsersToday", 0));
                    tPendingRequests.setText(sharedPreferences.getInt("pendingRequests", 0));
                }
                catch (Exception e)
                {
                    Toast.makeText(getActivity(), R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();
                }
            }
            else
            {
                try
                {
                    jsonObject = new JSONObject(message);
                    pendingOrders = jsonObject.getInt("saved");
                    processedOrders = jsonObject.getInt("processed");
                    cancelledOrders = jsonObject.getInt("cancelled");
                    totalUsers = jsonObject.getInt("totalUsers");
                    newUsersToday = jsonObject.getInt("newUsersToday");
                    pendingRequests = jsonObject.getInt("pendingUsers");

                    tPendingOrders.setText(pendingOrders + "");
                    tProcessedOrders.setText(processedOrders + "");
                    tCancelledOrders.setText(cancelledOrders + "");
                    tTotalUsers.setText(totalUsers + "");
                    tNewUsersToday.setText(newUsersToday + "");
                    tPendingRequests.setText(pendingRequests + "");

                    if(sharedPreferences.contains("pendingOrders") && pendingOrders > sharedPreferences.getInt("pendingOrders", 0) && sharedPreferences.getInt("pendingOrders", 0) != 0)
                        po = true;
                    if(sharedPreferences.contains("processedOrders") && processedOrders > sharedPreferences.getInt("processedOrders", 0) && sharedPreferences.getInt("pendingOrders", 0) != 0)
                        pro = true;
                    if(sharedPreferences.contains("cancelledOrders") && cancelledOrders > sharedPreferences.getInt("cancelledOrders", 0) && sharedPreferences.getInt("pendingOrders", 0) != 0)
                        co = true;
                    if(sharedPreferences.contains("totalUsers") && totalUsers > sharedPreferences.getInt("totalUsers", 0) && sharedPreferences.getInt("pendingOrders", 0) != 0)
                        tu = true;
                    if(sharedPreferences.contains("newUsersToday") && newUsersToday > sharedPreferences.getInt("newUsersToday", 0) && sharedPreferences.getInt("pendingOrders", 0) != 0)
                        nut = true;
                    if(sharedPreferences.contains("pendingRequests") && pendingRequests > sharedPreferences.getInt("pendingRequests", 0) && sharedPreferences.getInt("pendingOrders", 0) != 0)
                        pr = true;

                    updatePO();
                    updatePRO();
                    updateCO();
                    updateTU();
                    updateNUT();
                    updatePR();

                    editor.putInt("pendingOrders", pendingOrders);
                    editor.putInt("processedOrders", processedOrders);
                    editor.putInt("cancelledOrders", cancelledOrders);
                    editor.putInt("totalUsers", totalUsers);
                    editor.putInt("newUsersToday", newUsersToday);
                    editor.putInt("pendingRequests", pendingRequests);
                    editor.commit();
                }
                catch (JSONException e) {}
            }
        }
    }
}