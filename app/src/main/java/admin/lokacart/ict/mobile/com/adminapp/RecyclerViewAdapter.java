package admin.lokacart.ict.mobile.com.adminapp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import java.util.ArrayList;

/**
 * Created by Vishesh on 04-01-2016.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.DataObjectHolder>
{
    static String LOG_TAG = "RecyclerViewAdapter";
    ArrayList<String> mDataset;
    Context context;

    public static class DataObjectHolder extends RecyclerView.ViewHolder
    {
        TextView tDisplayText;
        MyListener callback;

        public DataObjectHolder(View itemView, Context context)
        {
            super(itemView);
            tDisplayText = (TextView) itemView.findViewById(R.id.tDisplayText);
            callback = (MyListener) context;
        }
    }

    public RecyclerViewAdapter(ArrayList<String> myDataset, Context context)
    {
        mDataset = myDataset;
        this.context = context;
    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        DataObjectHolder dataObjectHolder = new DataObjectHolder(view, context);
        return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position)
    {
        holder.tDisplayText.setText(mDataset.get(position));
        holder.tDisplayText.setLongClickable(true);
    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
