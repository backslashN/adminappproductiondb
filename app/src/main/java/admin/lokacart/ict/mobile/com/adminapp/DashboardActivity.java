package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by Vishesh on 29/12/15.
 */

import admin.lokacart.ict.mobile.com.adminapp.fragments.CancelledOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.ExistingUserFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.PendingOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.PendingRequestFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.ProcessedOrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.SettingsFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.OrderFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.MemberFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.DashboardFragment;
import admin.lokacart.ict.mobile.com.adminapp.fragments.ProductTypeFragment;
import android.content.BroadcastReceiver;
import android.content.IntentFilter;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.*;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener,
        MyListener, NavigationItemListener{

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Master master;
    FragmentManager fragmentManager;
    Bundle args = new Bundle();
    private static String LOG_TAG = "DashboardActivity";
    public static TextView tDashboardMobileNumber, tDashboardName, tDashboardEmail;
    public static boolean dashboardUpdate, onBackDashboard;
    public String dashboard_TAG;
    public static Boolean backPress;
    private final String TAG = "DashboardActivity";
    public static final String MEMBER_TAG = "Member", PRODUCT_TAG = "Product";
    NavigationView navigationView;
    static FloatingActionButton fab;


    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    public static void resetBackPress()
    {
        backPress = false;
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(MyGcmListenerService.ACTION));
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor;
        if(sharedPrefs.contains("orderNotifs"))
        {
            editor = sharedPrefs.edit();
            editor.remove("orderNotifs");
            editor.apply();
        }
        if(sharedPrefs.contains("memberNotifs"))
        {
            editor = sharedPrefs.edit();
            editor.remove("memberNotifs");
            editor.apply();
        }
        Master.getAdminData(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    public Context getContext()
    {
        return DashboardActivity.this;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Master.getAdminData(getApplicationContext());
        onBackDashboard = false;
        dashboard_TAG = "DASHBOARD_TAG";
        backPress = false;
        dashboardUpdate = false;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Intent intent = new Intent(this, RegistrationIntentService.class);
        if (checkPlayServices())
        {
            startService(intent);
        }

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent.getAction().equals(MyGcmListenerService.ACTION)) {

                    try
                    {
                        int params[] = new int[6];
                        params[0] = Integer.parseInt(intent.getStringExtra("saved"));
                        params[1] = Integer.parseInt(intent.getStringExtra("processed"));
                        params[2] = Integer.parseInt(intent.getStringExtra("cancelled"));
                        params[3] = Integer.parseInt(intent.getStringExtra("totalUsers"));
                        params[4] = Integer.parseInt(intent.getStringExtra("newUsersToday"));
                        params[5] = Integer.parseInt(intent.getStringExtra("pendingUsers"));

                        DashboardFragment dashboardFragment = (DashboardFragment) getSupportFragmentManager().findFragmentByTag(dashboard_TAG);
                        if(dashboardFragment != null && dashboardFragment.isVisible())
                        {
                            dashboardFragment.update(params);
                        }
                        else
                        {
                        }
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(DashboardActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        };
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPreferences.edit();
        master = new Master();

        tDashboardEmail = (TextView) findViewById(R.id.tDashboardEmail);
        tDashboardEmail.setText(AdminDetails.getEmail());

        tDashboardName = (TextView) findViewById(R.id.tDashboardName);
        tDashboardName.setText(AdminDetails.getName());

        tDashboardMobileNumber = (TextView) findViewById(R.id.tDashboardMobileNumber);
        tDashboardMobileNumber.setText(AdminDetails.getMobileNumber());

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MemberFragment memberFragment = (MemberFragment)getSupportFragmentManager().findFragmentByTag(MEMBER_TAG);
                if(memberFragment != null && memberFragment.isVisible())
                {
                    memberFragment.addNewMember();
                }
                else
                {
                    ProductTypeFragment productTypeFragment = (ProductTypeFragment)getSupportFragmentManager().findFragmentByTag(PRODUCT_TAG);
                    productTypeFragment.addNewProductType();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);

        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
    }
    public static void hideFAB()
    {
        fab.setVisibility(View.GONE);
    }
    public static void viewFAB()
    {
        fab.setVisibility(View.VISIBLE);
    }

    public static ArrayList<TextView> getDrawerTextViews()
    {
        ArrayList<TextView> textViews = new ArrayList<TextView>();
        textViews.add(0, tDashboardName);
        textViews.add(1, tDashboardEmail);
        textViews.add(2, tDashboardMobileNumber);
        return textViews;
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else if(onBackDashboard)
        {
            onBackDashboard = false;
            fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
        }
        else if(backPress)
        {
            finish();
        }
        else
        {
            CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, R.string.label_toast_press_back_once_more_to_exit, Snackbar.LENGTH_SHORT);
            snackbar.show();
            backPress = true;
        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        resetBackPress();
        onBackDashboard = false;

        int id = item.getItemId();

        if(id == R.id.nav_dashboard) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(0).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new DashboardFragment(), dashboard_TAG).commit();
            dashboardUpdate = false;

        } else if (id == R.id.nav_products) {
            fab.setVisibility(View.VISIBLE);
            navigationView.getMenu().getItem(1).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new ProductTypeFragment(), PRODUCT_TAG).commit();

        } else if (id == R.id.nav_orders) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(2).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new OrderFragment()).commit();

        } else if (id == R.id.nav_members) {
            navigationView.getMenu().getItem(3).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new MemberFragment(), MEMBER_TAG).commit();

        } else if (id == R.id.nav_settings) {
            fab.setVisibility(View.GONE);
            navigationView.getMenu().getItem(4).setChecked(true);
            fragmentManager.beginTransaction().replace(R.id.content_frame, new SettingsFragment()).commit();

        } else if (id == R.id.nav_logout) {
            if(Master.isNetworkAvailable(DashboardActivity.this))
            {
                navigationView.getMenu().getItem(5).setChecked(true);
                editor.putBoolean("login", false);
                editor.commit();
                JSONObject jsonObject = new JSONObject();
                try
                {
                    jsonObject.put("token", Master.getToken());
                }
                catch (JSONException e)
                {
                }
                new GCMDeregisterTask(DashboardActivity.this).execute(jsonObject);
                startActivity(new Intent(DashboardActivity.this, MainActivity.class));
                finish();
            }
            else
            {
                Master.alertDialog(DashboardActivity.this, getString(R.string.label_alertdialog_cannot_log_out_without_internet), "OK");
            }
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void itemSelected(int itemIndex) {
        navigationView.getMenu().getItem(itemIndex).setChecked(true);
        if(itemIndex == 1 || itemIndex == 3)
            fab.setVisibility(View.VISIBLE);
        else
            fab.setVisibility(View.GONE);
    }

    @Override
    public void onCardClickListener(int position,int cat,Object obj) {
        args.clear();
        if(cat == Master.savedToProcessedKey)
        {
            PendingOrderFragment pendingOrderFragment = (PendingOrderFragment) obj;
            pendingOrderFragment.clickListener(position);
        }
        else if(cat== Master.cancelledOrderKey)
        {
            CancelledOrderFragment cancelledOrderFragment = (CancelledOrderFragment)obj;
            cancelledOrderFragment.clickListener(position);
        }
        else if(cat == Master.existingUserClickKey)
        {
            ExistingUserFragment existingUserFragment = (ExistingUserFragment) obj;
            existingUserFragment.clickListener(position);
        }
        else if(cat == Master.pendingUserRequestKey)
        {
            PendingRequestFragment pendingRequestFragment = (PendingRequestFragment) obj;
            pendingRequestFragment.clickListener(position);
        }
        else if(cat== Master.processedOrderKey)
        {
            ProcessedOrderFragment processedOrderFragment = (ProcessedOrderFragment)obj;
            processedOrderFragment.clickListener(position);
        }
        else if(cat == Master.productTypeClickKey)
        {
            ProductTypeFragment productTypeFragment = (ProductTypeFragment) obj;
            productTypeFragment.clickListener(position);
        }
    }

    @Override
    public void onCardLongClickListener(final int position, int category, Object obj)
    {
        if(category == Master.productTypeClickKey)
        {
            ProductTypeFragment productTypeFragment = (ProductTypeFragment) obj;
            productTypeFragment.longClickListener(position);
        }
        else
        {
        }
    }

    class GCMDeregisterTask extends AsyncTask<JSONObject, Void, Void> {

        Context context;
        GCMDeregisterTask(Context context)
        {
            this.context = context;
        }
        protected Void doInBackground(JSONObject... params) {
            GetJSON getJson = new GetJSON();
            String response = getJson.getJSONFromUrl(Master.getDeregisterTokenURL(),params[0],"POST",false,null,null);
            return null;
        }
    }

}