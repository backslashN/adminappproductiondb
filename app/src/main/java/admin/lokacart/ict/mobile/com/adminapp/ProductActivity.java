package admin.lokacart.ict.mobile.com.adminapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ProductActivity extends AppCompatActivity implements MyListener{

    Dialog dialog;

    public RecyclerView mRecyclerView;
    public RecyclerView.Adapter mAdapter;
    public RecyclerView.LayoutManager mLayoutManager;

    public int onResumePosition;

    DashboardActivity dashboardActivity;
    //ArrayList<String> productList, priceList, quantityList;
    JSONObject responseObject;
    public static int recyclerViewIndex;

    String LOG_TAG, productType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Master.getAdminData(getApplicationContext());
        LOG_TAG = "Product Activity";

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                addProduct();
            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        productType = getIntent().getStringExtra("product type");
        setTitle(productType);

        mRecyclerView = (RecyclerView) findViewById(R.id.productRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ProductActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        /*productList = new ArrayList<>();
        quantityList = new ArrayList<>();
        priceList = new ArrayList<>();*/

        Master.productList = new ArrayList<>();
        Master.quantityList = new ArrayList<>();
        Master.priceList = new ArrayList<>();

        recyclerViewIndex = 0;

        new GetProductDetailsTask(productType).execute();

        dashboardActivity = new DashboardActivity();

    }

    @Override
    protected void onResume() {
        super.onResume();
        Master.getAdminData(getApplicationContext());
    }



    @Override
    public void onCardClickListener(int position, int cat, Object obj) {
        if(cat == Master.productClickKey)
        {
            //Log.e("Vish", productList.get(position) + " clicked");
            //modifyProduct(position);
            Intent i = new Intent(ProductActivity.this, EditProductActivity.class);
            /*i.putStringArrayListExtra("productList", productList);
            i.putStringArrayListExtra("priceList", priceList);
            i.putStringArrayListExtra("quantityList", quantityList);*/
            i.putExtra("position", position);
            i.putExtra("size", recyclerViewIndex);
            onResumePosition = position;
            startActivityForResult(i, 0);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Master.getAdminData(getApplicationContext());
        mAdapter.notifyItemChanged(onResumePosition);
    }

    @Override
    public void onCardLongClickListener(int position, int category, Object obj) {
        if(category == Master.productClickKey)
        {
            //Log.e("Vish", productList.get(position) + " Long clicked");
            modifyProduct(position);
        }
    }

    /********************************A class to fetch products from the server.********************/
    public class GetProductDetailsTask extends AsyncTask<String, String, String> {
        ProgressDialog pd;
        String response, productType;

        public GetProductDetailsTask(String productType) {
            this.productType = productType;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(ProductActivity.this);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String getProductURL = Master.getProductURL() + AdminDetails.getAbbr();
            GetJSON jParser = new GetJSON();
            response = jParser.getJSONFromUrl(getProductURL, null, "GET", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            pd.dismiss();
            if (response.equals("exception"))
            {
                Master.alertDialog(ProductActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
            else
            {
                try
                {
                    JSONObject jsonObject;
                    responseObject = new JSONObject(response);
                    JSONArray jsonArray = responseObject.getJSONArray(productType);
                    if(jsonArray.length() > 0)
                    {
                        for (recyclerViewIndex = 0; recyclerViewIndex < jsonArray.length(); ++recyclerViewIndex)
                        {
                            jsonObject = (JSONObject) jsonArray.get(recyclerViewIndex);
                            /*productList.add(recyclerViewIndex, jsonObject.getString("name"));
                            priceList.add(recyclerViewIndex, jsonObject.getString("unitRate"));
                            quantityList.add(recyclerViewIndex, jsonObject.getString("quantity"));*/

                            Master.productList.add(recyclerViewIndex, jsonObject.getString("name"));
                            Master.priceList.add(recyclerViewIndex, jsonObject.getString("unitRate"));
                            Master.quantityList.add(recyclerViewIndex, jsonObject.getString("quantity"));
                        }

                        //mAdapter = new ProductRecyclerViewAdapter(productList, quantityList, priceList, ProductActivity.this);
                        mAdapter = new ProductRecyclerViewAdapter(Master.productList, Master.quantityList, Master.priceList, ProductActivity.this);
                        mRecyclerView.setAdapter(mAdapter);
                        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ProductActivity.this, mRecyclerView, Master.productClickKey, null));
                    }
                    else
                    {
                        alertDialog();
                    }
                }
                catch (Exception e)
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        getSupportFragmentManager().popBackStack();
        return true;
    }

    /*********************************End of class*************************************************/


    public void alertDialog()
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(ProductActivity.this);

        builder.setMessage(R.string.label_there_are_no_products_in_this_product_type);
        builder.setCancelable(false);

        builder.setPositiveButton(
                "ADD",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        addProduct();
                        dialog.cancel();
                    }
                });

        builder.setNegativeButton(
                "BACK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert11 = builder.create();
        alert11.show();
    }
    private void addProduct() {

        dialog = new Dialog(ProductActivity.this);
        dialog.setContentView(R.layout.product_box);
        dialog.setTitle(R.string.dialog_title_add_product);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eProductName, eProductPrice, eProductQuantity;
        final Button bProductConfirm, bProductCancel, bProductDelete, bProductEdit;

        eProductName = (EditText) dialog.findViewById(R.id.eProductName);
        eProductPrice = (EditText) dialog.findViewById(R.id.eProductPrice);
        eProductQuantity = (EditText) dialog.findViewById(R.id.eProductQuantity);

        bProductEdit = (Button) dialog.findViewById(R.id.bProductEdit);
        bProductEdit.setVisibility(View.GONE);
        bProductDelete = (Button) dialog.findViewById(R.id.bProductDelete);
        bProductDelete.setVisibility(View.GONE);

        bProductConfirm = (Button) dialog.findViewById(R.id.bProductConfirm);
        bProductCancel = (Button) dialog.findViewById(R.id.bProductCancel);

        bProductCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bProductConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO send the new product name
                String newProductName, newProductPrice, newProductQuantity;
                newProductName = eProductName.getText().toString().trim();
                newProductPrice = eProductPrice.getText().toString().trim();
                newProductQuantity = eProductQuantity.getText().toString().trim();

                if (newProductName.equals("")) {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_a_product_name, Toast.LENGTH_SHORT).show();
                } else if (newProductQuantity.equals("")) {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_quantity, Toast.LENGTH_SHORT).show();
                } else if (newProductPrice.equals("")) {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_price, Toast.LENGTH_SHORT).show();
                } else if (!Master.isNetworkAvailable(ProductActivity.this)) {
                    Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                } else {

                    boolean flag = true;
                    for (int i = 0; i < recyclerViewIndex; ++i) {
                        /*if (productList.get(i).equals(newProductName)) {
                            flag = false;
                            break;
                        }*/
                        if (Master.productList.get(i).equals(newProductName)) {
                            flag = false;
                            break;
                        }
                    }
                    if (flag) {
                        JSONObject jsonObject = new JSONObject();
                        try {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("name", newProductName);
                            jsonObject.put("productType", productType);
                            jsonObject.put("rate", newProductPrice);
                            jsonObject.put("qty", newProductQuantity);
                        } catch (JSONException e) {
                        }
                        new AddProductTask(ProductActivity.this, newProductName, newProductPrice, newProductQuantity).execute(jsonObject);
                    }
                    else
                    {
                        Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_unique_product, Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }
// --------------------End of addProduct function---------------------------------------------------


//---------------------AddProduct Asynctask---------------------------------------------------------

    public void modifyProduct(final int position)
    {
        final String productName, productPrice, productQuantity;
        /*productName = productList.get(position);
        productPrice = priceList.get(position);
        productQuantity = quantityList.get(position);*/

        productName = Master.productList.get(position);
        productPrice = Master.priceList.get(position);
        productQuantity = Master.quantityList.get(position);

        dialog = new Dialog(ProductActivity.this);
        dialog.setContentView(R.layout.product_box);
        dialog.setTitle("Modify " + productName);
        dialog.show();
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final EditText eProductName, eProductPrice, eProductQuantity;
        final Button bProductConfirm, bProductCancel, bProductDelete, bProductEdit;

        eProductName = (EditText) dialog.findViewById(R.id.eProductName);
        eProductName.setVisibility(View.GONE);

        eProductPrice = (EditText) dialog.findViewById(R.id.eProductPrice);
        eProductPrice.setVisibility(View.GONE);

        eProductQuantity = (EditText) dialog.findViewById(R.id.eProductQuantity);
        eProductQuantity.setVisibility(View.GONE);

        bProductEdit = (Button) dialog.findViewById(R.id.bProductEdit);
        bProductDelete = (Button) dialog.findViewById(R.id.bProductDelete);

        bProductConfirm = (Button) dialog.findViewById(R.id.bProductConfirm);
        bProductConfirm.setVisibility(View.GONE);

        bProductCancel = (Button) dialog.findViewById(R.id.bProductCancel);

        bProductCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        bProductEdit.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                eProductName.setVisibility(View.VISIBLE);
                eProductName.setText(productName);
                eProductPrice.setVisibility(View.VISIBLE);
                eProductPrice.setText(productPrice);
                eProductQuantity.setVisibility(View.VISIBLE);
                eProductQuantity.setText(productQuantity);

                bProductEdit.setVisibility(View.GONE);
                bProductDelete.setVisibility(View.GONE);
                bProductConfirm.setVisibility(View.VISIBLE);
            }
        });

        bProductConfirm.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                String enteredName, enteredPrice, enteredQuantity;
                enteredName = eProductName.getText().toString().trim();
                enteredPrice = eProductPrice.getText().toString().trim();
                enteredQuantity = eProductQuantity.getText().toString().trim();

                if(enteredName.equals(""))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_a_product_name,Toast.LENGTH_SHORT).show();
                }
                else if(enteredPrice.equals(""))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_price,Toast.LENGTH_SHORT).show();
                }
                else if(enteredQuantity.equals(""))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_quantity,Toast.LENGTH_SHORT).show();
                }
                else
                {
                    boolean flag = false;
                    for(int i = 0; i < recyclerViewIndex; ++i)
                    {
                        if(i == position) // Skip checking the same product
                            continue;
                        /*if(productList.get(i).equals(enteredName))
                        {
                            flag = true;
                            break;
                        }*/
                        if(Master.productList.get(i).equals(enteredName))
                        {
                            flag = true;
                            break;
                        }
                    }
                    if(!flag)
                    {
                        JSONObject jsonObject = new JSONObject();
                        try
                        {
                            jsonObject.put("orgabbr", AdminDetails.getAbbr());
                            jsonObject.put("name", productName);
                            jsonObject.put("newname", enteredName);
                            jsonObject.put("qty", enteredQuantity);
                            jsonObject.put("rate", enteredPrice);
                        }
                        catch(JSONException e)
                        {
                            Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
                        }
                        if(Master.isNetworkAvailable(ProductActivity.this))
                        {
                            new EditProductTask(ProductActivity.this, position, enteredName, enteredPrice, enteredQuantity).execute(jsonObject);
                            dialog.dismiss();
                        }
                        else
                        {
                            Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                        }
                    }
                    else
                    {
                        Toast.makeText(ProductActivity.this, R.string.label_toast_please_enter_unique_product, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        bProductDelete.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v)
            {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("orgabbr", AdminDetails.getAbbr());
                    jsonObject.put("name", productName);
                }
                catch (JSONException e)
                {
                }
                if(Master.isNetworkAvailable(ProductActivity.this))
                {
                    new DeleteProductTask(ProductActivity.this, productName, position).execute(jsonObject);
                    dialog.dismiss();
                }
                else
                {
                    Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_internet, Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.show();
    }


//---------------------EditProduct Asynctask---------------------------------------------------------
    /******************************************************************
    API:
        /api/product/edit

        body:
        {
          "orgabbr":"Test2",
          "name":"Pumpkin",
           "newname":"Pumpkins",
           "qty":"120",
            "rate":"125"
        }

        response:
        {
            "edit": "success"
        }
     *******************************************************************/
public class EditProductTask extends AsyncTask<JSONObject, String, String>
{
    Context context;
    String response, productName, productPrice, productQuantity;
    ProgressDialog pd;
    int position;

    EditProductTask(Context context, int position, String productName, String productPrice, String productQuantity)
    {
        this.context = context;
        this.productName = productName;
        this.productPrice = productPrice;
        this.productQuantity = productQuantity;
        this.position = position;
    }

    @Override
    protected void onPreExecute() {
        pd = new ProgressDialog(context);
        pd.setMessage(getString(R.string.label_please_wait));
        pd.setCancelable(false);
        pd.show();
    }

    @Override
    protected String doInBackground(JSONObject... params) {
        GetJSON getJSON = new GetJSON();
        response = getJSON.getJSONFromUrl(Master.getEditProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
        return response;
    }

    @Override
    protected void onPostExecute(String message) {
        pd.dismiss();
        try
        {
            JSONObject jsonObject = new JSONObject(message);
            if(jsonObject.getString("edit").toString().equals("success"))
            {
                Toast.makeText(ProductActivity.this, R.string.label_toast_Product_updated_successfully, Toast.LENGTH_SHORT);
                /*productList.set(position, productName);
                priceList.set(position, productPrice);
                quantityList.set(position, productQuantity);*/

                Master.productList.set(position, productName);
                Master.priceList.set(position, productPrice);
                Master.quantityList.set(position, productQuantity);

                mAdapter.notifyItemChanged(position);
            }
            else
            {
                Master.alertDialog(ProductActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
            }
        }
        catch (JSONException e)
        {
            Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
        }

    }
}
//---------------------End of EditProduct Asynctask-------------------------------------------------

//---------------------DeleteProduct Asynctask---------------------------------------------------------
    /******************************************************************
     API:
     /api/product/delete

     body:
     {
        "orgabbr":"Test2",
        "name":"Pumpkin",
     }

     response:
     {
        "result": "Delete successful"
            OR
        "result": "failed to delete"
     }
     *******************************************************************/
    public class DeleteProductTask extends AsyncTask<JSONObject, String, String>
    {
        Context context;
        String response, productName;
        ProgressDialog pd;
        int position;

        DeleteProductTask(Context context, String productName, int position)
        {
            this.context = context;
            this.productName = productName;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getDeleteProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            pd.dismiss();
            try
            {
                JSONObject jsonObject = new JSONObject(message);
                if(jsonObject.getString("result").equals("Delete successful"))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_product_deleted_successfully, Toast.LENGTH_SHORT);
                    /*productList.remove(position);
                    priceList.remove(position);
                    quantityList.remove(position);*/

                    Master.productList.remove(position);
                    Master.priceList.remove(position);
                    Master.quantityList.remove(position);

                    mAdapter.notifyItemRemoved(position);
                    recyclerViewIndex--;
                    if(recyclerViewIndex <= 0)
                    {
                        alertDialog();
                    }
                }
                else
                {
                    Master.alertDialog(ProductActivity.this, getString(R.string.label_toast_cannot_delete_product), "OK");
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(ProductActivity.this, R.string.label_cannot_connect_to_the_server, Toast.LENGTH_SHORT).show();
            }

        }
    }
//---------------------End of DeleteProduct Asynctask-------------------------------------------------


    //---------------------AddProduct Asynctask---------------------------------------------------------
    public class AddProductTask extends AsyncTask<JSONObject, String, String>
    {
        Context context;
        String response, productName, productPrice, productQuantity;
        ProgressDialog pd;

        AddProductTask(Context context, String productName, String productPrice, String productQuantity)
        {
            this.context = context;
            this.productName = productName;
            this.productPrice = productPrice;
            this.productQuantity = productQuantity;
        }

        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
            GetJSON getJSON = new GetJSON();
            response = getJSON.getJSONFromUrl(Master.getAddNewProductURL(), params[0], "POST", true, AdminDetails.getEmail(), AdminDetails.getPassword());
            return response;
        }

        @Override
        protected void onPostExecute(String message) {
            pd.dismiss();
            try
            {
                JSONObject jsonObject = new JSONObject(message);
                if(jsonObject.getString("upload").equals("success"))
                {
                    Toast.makeText(ProductActivity.this, R.string.label_toast_Product_added_successfully, Toast.LENGTH_SHORT);
                    /*productList.add(recyclerViewIndex, productName);
                    priceList.add(recyclerViewIndex, productPrice);
                    quantityList.add(recyclerViewIndex, productQuantity);*/

                    Master.productList.add(recyclerViewIndex, productName);
                    Master.priceList.add(recyclerViewIndex, productPrice);
                    Master.quantityList.add(recyclerViewIndex, productQuantity);

                    if(recyclerViewIndex == 0) // recycler view not initialised
                    {
                        //mAdapter = new ProductRecyclerViewAdapter(productList, quantityList, priceList, ProductActivity.this);

                        mAdapter = new ProductRecyclerViewAdapter(Master.productList, Master.quantityList, Master.priceList, ProductActivity.this);

                        mRecyclerView.setAdapter(mAdapter);
                        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(ProductActivity.this, mRecyclerView, Master.productClickKey, null));
                    }
                    else
                    {
                        mAdapter.notifyItemInserted(recyclerViewIndex++);
                    }
                    dialog.dismiss();
                }
                else
                {
                    Master.alertDialog(ProductActivity.this, getString(R.string.label_cannot_connect_to_the_server), "OK");
                }
            }
            catch (JSONException e)
            {
                Toast.makeText(ProductActivity.this, R.string.label_toast_something_went_worng, Toast.LENGTH_SHORT).show();
            }

        }
    }
//--------------------------End of AddProductTask -----------------------------------------------


}
