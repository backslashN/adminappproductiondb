package admin.lokacart.ict.mobile.com.adminapp;

/**
 * Created by Vishesh on 25-01-2016.
 */
public interface NavigationItemListener {
    public void itemSelected(int itemIndex);
}
